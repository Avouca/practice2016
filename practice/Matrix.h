#pragma once
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <stdio.h>

namespace Practice 
{
	using namespace std;

	template<class T>
	struct Matrix
	{
		Matrix(vector<vector<T>> d, int w, int h) : data(d), width(w), height(h) {}

		vector<vector<T>> data = {};

		unsigned int width  = 0;   // ������ �������
		unsigned int height = 0;   // ����� �������

		T get(int i, int j) 
		{
			return data[i][j];
		}

		bool isValid() 
		{
			if (data.size() != height)
			return false;

			for (int i = 0; i < height; i++) 
			{
				auto line = data[i];
				if (line.size() != width)
				return false;
			}
			return true;
		}

		int strWidth(T x)
		{
			return toStr(x).size();
		}

		string toStr(T data) 
		{
			std::ostringstream oss;
			oss << data;
			return oss.str();
		}

		string toString() 
		{
			stringstream buffer;

			if (!isValid()) 
			{
				buffer << " Matrix not valid " << data.size();
				return buffer.str();
			}

			int columnWidth = 0;


			for (int i = 0; i < height; i++) 
			{
				for (int j = 0; j < width; j++) 
				{
					int num = strWidth(get(i, j));
					if (columnWidth < num) 
					{
						columnWidth = num;
					}
				}
			}

			for (int i = 0; i < height; i++) 
			{
				for (int j = 0; j < width; j++) 
				{
					buffer << setw(columnWidth + 1) << get(i, j);
				}
				buffer << endl;
			}

			return buffer.str();
		}

		void show() 
		{
			cout << toString();
		}

	};

	typedef Matrix<int> MatrixInt;
}