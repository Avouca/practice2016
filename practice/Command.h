﻿#pragma once
#include <string>
#include <iostream>

namespace Practice 
{
	using namespace std;

	struct Command 
	{
		virtual ~Command() {};
		virtual void exec() = 0;

		bool confirm() 
		{
			string input;
			while (true)
			{
				cout << " Подтверждение (Y - да, N - нет): ";
				getline(cin, input);

				cout << endl;
				if (input == "Y" || input == "y" || input == "")
				return true;

				else if (input == "N" || input == "n") 
				{
					cout << " Отменено." << endl;
					cout << " --------------------------------------------- " << endl;
					return false;
				}
			}
		}
	};
}