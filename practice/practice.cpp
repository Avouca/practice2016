﻿#include "stdafx.h"

#include "Matrix.h"
#include "CurrentData.h"
#include "Command.h"

#include "HelpCommand.h"
#include "PrintCommand.h"
#include "ReadCommand.h"
#include "GenCommand.h"
#include "ExitCommand.h"
#include "DeleteCommand.h"

#include "MaxModule.h"         // Максимум     (Еременко)
#include "MinModule.h"         // Минимум      (Смирнов)
#include "AverageModule.h"     // Среднее      (Ильченко)
#include "MajorityModule.h"    // Большинство  (Черкасова)
#include "SumModule.h"         // Сумма        (Тощиков)
#include "DiversityModule.h"   // Разнообразие (Петоухов)
#include "DeviationModule.h"   // Отклонение   (Щеколдин)
#include "RangeModule.h"       // Диапазон     (Голубков)         

using namespace std;
using namespace Practice; 

void menuLoop()                              
{
	string input = " ";                        
	map<string, Command*> commands;            
												 
	commands["gen"]   = new GenCommand();
	commands["exit"]  = new ExitCommand();
	commands["help"]  = new HelpCommand();
	commands["print"] = new PrintCommand();
	commands["read"]  = new ReadCommand();
	commands["del"]   = new DeleteCommand();
	
	commands["maj"]   = new MajorityModule();
	commands["max"]   = new MaxModule();
	commands["avr"]   = new AverageModule();
	commands["min"]   = new MinModule();
	commands["sum"]   = new SumModule();
	commands["div"]   = new DiversityModule();
	commands["dev"]   = new DeviationModule();
	commands["ran"]   = new RangeModule();

	while (true)
	{
		cout << " Введите команду: ";
		getline(cin, input);
		
		if (input == "") // nothing to do
			continue;

		if (commands.find(input) != commands.end()) 
		{
			cout << " --------------------------------------------------------------- " << endl;
			commands[input]->exec();
			cout << " --------------------------------------------------------------- " << endl;
		}
		else 
		{
			cout << " '" << input << "' - Такой команды нет" << endl;
			cout << " --------------------------------------------------------------- " << endl;
		}
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");

	cout << endl;
	cout << "              СУРГУТСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ"              << endl << endl;
	cout << "     Практическая работа: Летняя практика"                         << endl;
	cout << "                  Задача: Генерация, заполнение двумерных"         << endl;
	cout << "                          массивов и операции над ними"            << endl;
	cout << "                 Кафедра: Прикладная математика и информатика"     << endl;
	cout << "               Выполнили: Студенты группы 601-41 "                 << endl;
	cout << "                Проверил: Назин Антон Георгиевич"                  << endl << endl;

	cout << " --------------------------------------------------------------- " << endl;
	(new HelpCommand())->exec();
	cout << " --------------------------------------------------------------- " << endl;
	menuLoop();
    return 0;
}

