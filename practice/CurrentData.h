#pragma once
#include "Matrix.h"

namespace Practice 
{
	using namespace std;

	class CurrentData
	{
		public:
		int numberOfArrays = 0;
		int width;
		int height;

		vector<MatrixInt> matrices;

		static CurrentData& Instance()
		{
			static CurrentData s;
			return s;
		}

		private:
		CurrentData() {}
		~CurrentData() {}

		CurrentData(CurrentData const&);
		CurrentData& operator= (CurrentData const&);
	};
}