#pragma once
#include "DoubleModule.h"

namespace Practice 
{
	struct AverageModule : DoubleModule 
	{
		void help()
		{
			cout << "  <�������>" << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������"            << endl;
			cout << "           ������� �������� �� ������� �������"                    << endl << endl;
		}

		double calc(InputData data) 
		{
			double sum = 0.0;

			for (int i = 0; i < data.size(); i++) 
			{
				sum += data[i];
			}

			return sum / data.size();
		}
	};
}
