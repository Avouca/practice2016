﻿#pragma once
#include <fstream>
#include "Command.h"
#include "CurrentData.h"

namespace Practice 
{
	using namespace std;

	struct ReadCommand : Command
	{
		CurrentData& dataInstance = CurrentData::Instance();

		inline bool fileExists(const std::string& name) 
		{
			struct stat buffer;
			return (stat(name.c_str(), &buffer) == 0);
		}

		void exec() 
		{
			int i = 0;
			int number;
			int w = 0, h = 0;
			int oldW = 0, oldH = 0;

			dataInstance.matrices.clear();

			while (fileExists(to_string(i + 1) + ".txt")) 
			{
				ifstream inputFile(to_string(i + 1) + ".txt");

				vector<vector<int>> matrixVector = {};

				std::string fileLine;
				while (std::getline(inputFile, fileLine))
				{
					vector<int> line = {};
					std::istringstream iss(fileLine);

					while (iss >> number) 
					{
						line.push_back(number);
					}

					w = line.size();
					matrixVector.push_back(line);
				}

				h = matrixVector.size();

				MatrixInt matrix(matrixVector, w, h);

				if (i > 0 && (oldW != w || oldH != h)) 
				{
					cout << " Ошибка: массивы разных размерностей" << endl;
					return;
				}

				if (matrix.isValid()) 
				{
					dataInstance.matrices.push_back(matrix);
					oldW = w;
					oldH = h;
				}
				else 
				{
					cout << " Массив №" << (i + 1) << " некорректен" << endl;
				}

				i++;
			}

			dataInstance.numberOfArrays = i;
			dataInstance.width = w;
			dataInstance.height = h;

			cout << "  <Считывание массива(ов)>" << endl << endl;
			cout << " Массивы успешно считаны."  << endl;
		}
	};
}