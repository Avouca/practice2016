﻿#pragma once
#include "Command.h"
#include "CurrentData.h"

namespace Practice 
{
	using namespace std;

	struct PrintCommand : Command 
	{
		CurrentData& dataInstance = CurrentData::Instance();
		void exec() 
		{
			cout << "  <Вывод массива(ов)>" << endl << endl;

			for (int i = 0; i < dataInstance.numberOfArrays; i++) 
			{
				cout << " Массив №" << (i + 1) << ":" << endl;
				dataInstance.matrices[i].show();
				cout << endl;
			}

			cout << " Вывод массива(ов) завершен." << endl;
		}
	};
}