#pragma once
#include "Command.h"

namespace Practice 
{
	template<class Output>
	struct Module : Command 
	{
		typedef vector<int> InputData;

		CurrentData& dataInstance = CurrentData::Instance();

		virtual void help() = 0;

		virtual Output calc(InputData data) = 0;

		void exec()
		{
			help();

			vector<vector<Output>> matrixVector(dataInstance.height);

			for (int i = 0; i < dataInstance.height; i++)
			{
				vector<Output> line(dataInstance.width);

				for (int j = 0; j < dataInstance.width; j++)
				{
					vector<int> params(dataInstance.numberOfArrays);

					for (int k = 0; k < dataInstance.numberOfArrays; k++)
					{
						params[k] = dataInstance.matrices[k].get(i, j);
					}

					line[j] = calc(params);
				}
				matrixVector[i] = line;
			}

			Matrix<Output> matrix(matrixVector, dataInstance.width, dataInstance.height);

			matrix.show();
			cout << endl;
		}
	};
}