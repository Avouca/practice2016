#pragma once
#include "IntModule.h"

namespace Practice 
{
	struct MaxModule : IntModule 
	{
		void help()
		{
			cout << "  <��������>"                                                      << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������  "          << endl;
			cout << "           ������������ �������� �� ������� �������     "          << endl << endl;
		}

		int calc(InputData data) 
		{
			int max = data[1];

			for (int i = 0; i < data.size(); i++)
			{
				if (max < data[i])
				{
					max = data[i];
				}
			}
				
			return max;
		}
	};
}
