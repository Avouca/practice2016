#pragma once
#include "IntModule.h"

namespace Practice
{
	struct RangeModule : IntModule
	{
		void help()
		{
			cout << "  <��������>"                                                      << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������  "          << endl;
			cout << "           �������� �������� �� ������� �������"                   << endl << endl;
		}

		int calc(InputData data)
		{
			int max = data[1], min = data[1], dia;

			for (int i = 0; i < data.size(); i++)
			{
				if (min > data[i])
				{
					min = data[i];
			    }
			 
				if (max < data[i])
				{
					max = data[i];
				}
			}

			dia = max - min;
			return dia;
		}
	};
}