#pragma once
#include "IntModule.h"

namespace Practice 
{
	struct MinModule : IntModule
	{
		void help()
		{
			cout << "  <�������>"                                                       << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������  "          << endl;
			cout << "           ����������� �������� �� ������� �������      "          << endl << endl;
		}

		int calc(InputData data) 
		{
			int min = data[1];

			for (int i = 0; i < data.size(); i++)
			{
				if (min > data[i])
				{
					min = data[i];
				}
			}

			return min;
		}
	};
}
