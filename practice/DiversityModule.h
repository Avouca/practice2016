#pragma once
#include "IntModule.h"

namespace Practice 
{
	struct DiversityModule : IntModule 
	{
		void help()
		{
			cout << "  <������������>" << endl << endl;
			cout << " ��������: �������, ������������ ��� ������� ���������  "          << endl;
			cout << "           ���-�� ���������� �������� �� ������� �������"  << endl << endl;
		}

		int calc(InputData data) 
		{
			int diver = 0;

			for (int i = 0; i < data.size(); i++)
			{
				bool check = true;
				for (int g = i + 1; g < data.size(); g++)
				{
					check = check && (data[i] != data[g]);
				}
				if (check)
				{
					diver++;
				}
			}

			return diver;
		}
	};
}
