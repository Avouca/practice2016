#pragma once
#include "Module.h"

namespace Practice
{
	struct DoubleModule : Module<double>
	{
		virtual double calc(InputData data) = 0;
	};
}