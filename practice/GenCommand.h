﻿#pragma once
#include "Command.h"
#include "CurrentData.h"

namespace Practice 
{
	using namespace std;

	struct GenCommand : Command
	{
		CurrentData& dataInstance = CurrentData::Instance();

		int width, 
			height, 
			numberOfFiles, 
			minNumber, 
			maxNumber;

		void clean_stdin()
		{
			while (getchar() != '\n');
		}

		int readIntFromConsole() 
		{
			int result;

			while (!(cin >> result)) 
			{
				cin.clear();
				clean_stdin();
				cout << "Number incorrect. Please try again:\n";
			}

			return result;
		}

		void exec() 
		{
			cout << " <Генерация массива> " << endl;
			cout << endl;
			cout << "     Введите необх. кол-во строк: ";
			width = readIntFromConsole();

			cout << "  Введите необх. кол-во столбцов: ";
			height = readIntFromConsole();

			cout << "   Минимальное значение элемента: ";
			minNumber = readIntFromConsole();

			cout << "  Максимальное значение элемента: ";
			maxNumber = readIntFromConsole();

				if (minNumber > maxNumber) 
				{
					cout << endl;
					cout << " Ошибка: Минимальное число должно быть < макс." << endl << endl;
					getchar();
					return;
					
				}

			cout << "               Количество файлов: ";
			numberOfFiles = readIntFromConsole();
			cout << endl;

			getchar();

			if (confirm()) 
			{
				generate();
			}
		}


		int randBetween(int from, int to) 
		{
			return rand() % (to - from + 1) + from;
		}

		void generate() 
		{
			dataInstance.width = width;
			dataInstance.height = height;
			dataInstance.numberOfArrays = numberOfFiles;

			dataInstance.matrices.clear();

			for (int i = 0; i < numberOfFiles; i++) 
			{
				MatrixInt matrix(generateVector(), width, height);
				dataInstance.matrices.push_back(matrix);
				ofstream outputFile(to_string(i + 1) + ".txt");
				outputFile << matrix.toString();
			}

			cout << " Генерация завершена." << endl;
		}

		vector<vector<int>> generateVector() 
		{
			vector<vector<int>> result = {};
			for (int i = 0; i < height; i++) 
			{
				vector<int> line = {};
				for (int j = 0; j < width; j++) 
				{
				
					line.push_back(randBetween(minNumber, maxNumber));
					
				}
				result.push_back(line);
			}
			return result;
		}
	};
}





/*

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

void Turovec(int kol, int stroka, int stolb)
{
	int arr[stroka][stolb];
	string str;
	ofstream fout;

	unsigned rand_value = 11;
	srand(rand_value);

	for (int i = 0; i < kol; i++)
	{

		str = to_string(i + 1);
		fout.open(((str)+".txt"));

		fout << endl;
		fout << "       Кол-во строк: " << stroka << endl;
		fout << "    Кол-во столбцов: " << stolb << endl;
		fout << endl;
		fout << " Полученная матрица:" << endl;
		cout << endl;

		for (int i = 0; i < stroka; i++)
		{
			for (int j = 0; j < stolb; j++)
			{
				arr[i][j] = (1 + rand() % 10);
				fout << "   " << arr[i][j] << " ";
				cout << " " << arr[i][j] << '\t';
			}
			cout << endl;
			fout << '\n';
		}
		cout << endl;
		fout.close();
	}
}

int main()
{
	setlocale(LC_ALL, "rus");
	int stroka, kol, stolb;

	cout << endl;
	cout << "    Введите необходимое кол-во строк: ";
	cin >> stroka;

	cout << " Введите необходимое кол-во столбцов: ";
	cin >> stolb;

	cout << "   Введите необходимое кол-во файлов: ";
	cin >> kol;

	if (kol < 1)
	{
		cout << endl;
		cout << " --------------------------" << endl;
		cout << " Ни один файл не был создан" << endl;
		cout << " --------------------------" << endl;
		cout << endl;

		cout << " Попробуйте еще раз. " << endl;

		cout << "    Введите необходимое кол-во строк: ";
		cin >> stroka;

		cout << " Введите необходимое кол-во столбцов: ";
		cin >> stolb;

		cout << "   Введите необходимое кол-во файлов: ";
		cin >> kol;

		Turovec(kol, stroka, stolb);
	}
	else if (stolb < 1 && stroka < 1)
	{
		cout << endl;
		cout << " Создан(o) " << kol << " файл(ов) с матрицей(ами) размера 0х0 " << endl;

		Turovec(kol, stroka, stolb);
		cout << endl;
	}
	else if (stolb < 1 || stroka < 1)
	{
		cout << endl;
		cout << " Создан(o) " << kol << " файл(ов) с матрицей(ами) размера 0х0 " << endl;

		Turovec(kol, stroka, stolb);
		cout << endl;
	}
	else if (stolb >= 1 && stroka >= 1)
	{
		Turovec(kol, stroka, stolb);
	}

	system("pause");
	return 0;
}

*/