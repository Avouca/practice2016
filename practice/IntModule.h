﻿#pragma once
#include "Module.h"

namespace Practice 
{
	struct IntModule : Module<int>
	{
		virtual int calc(InputData data) = 0;
	};
}